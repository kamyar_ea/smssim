**SMS Panel Simulation**

In order to run the application follow the below steps:

- Create a DB named smssim_db with utf8_general_ci collation and apply your DB credentials in .env file

- Apply your redis server configurations in .env file

- Run php artisan migrate

- For retrying failed messages (sms) in 10 minutes intervals, add below cronjob line to crontab:

        10 * * * * php /(project_path)/artisan queue:work --stop-when-empty

- For calling sms sending api use below address:

        localhost/sms/send?number=989121111111&body=test

- For viewing reports panel use below address:

        localhost/sms/reports
        
- This application uses 2 APIs for sending sms. You can find and change API URLs in below path:

        smssim/app/Helpers/Loggers.php -> sms_url_maker function