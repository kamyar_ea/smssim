<?php

namespace App\Helpers;

use GuzzleHttp;

class ApiCaller
{

    public $error = [];

    public function api_call($url,$api_name)
    {

        $client = new GuzzleHttp\Client();

        try {

            $request = $client->get($url);

            $response = $request->getBody();

            $status = $request->getStatusCode();

            if ($status == 200){

                return true;

            } else {

                $this->error[] = "$api_name Error: ".$response;

            }

        } catch (GuzzleHttp\Exception\ConnectException $e){

            $this->error[] = "$api_name Error: ".$e->getMessage();

        }

        return false;

    }

}