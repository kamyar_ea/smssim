<?php

namespace App\Helpers;

use App\Api_usage;
use App\Log;

class Loggers
{

    // if status == 1, sms has been sent successfully.
    // if status == 0, sms has not been sent.
    public function log_sms($number,$body,$status)
    {

        $log = new Log;

        $log->number = $number;
        $log->body = $body;
        $log->status = $status;

        $log->save();

        return $log->id;

    }

    // if status == 1, sms has been sent successfully.
    // if status == 0, sms has not been sent.
    public function update_sms_log($id,$status)
    {

        $log = Log::find($id);

        $log->status = $status;

        $log->save();

    }

    // if status == 1, api has responded successfully.
    // if status == 0, api has been failed.
    public function log_api_usage($api_name,$status,$log_id,$error_text)
    {

        $api_usage = new Api_usage;

        $api_usage->api_name = $api_name;
        $api_usage->status = $status;
        $api_usage->log_id = $log_id;
        $api_usage->error_text = $error_text;

        $api_usage->save();

    }

    public function sms_url_maker($number,$body,$api_name)
    {

        if ($api_name == 'api_1'){

            $url = "localhost:81/sms/send?number=$number&body=$body";
            return $url;

        } elseif ($api_name == 'api_2'){

            $url = "localhost:82/sms/send?number=$number&body=$body";
            return $url;

        }

    }

}