<?php

namespace App\Http\Controllers;

use App\Api_usage;
use App\Helpers\ApiCaller;
use App\Helpers\Loggers;
use App\Jobs\SendFailedSms;
use App\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;

class SmsController extends Controller
{

    public function send(Request $request)
    {

        $number = $request->number;

        $body = $request->body;

        $validator = Validator::make(['number'=>$number],['number'=>'required|integer']);

        if ($validator->fails()){
            return response()->json(['message'=>'number is required and should be integer'],422);
        }

        $validator = Validator::make(['body'=>$body],['body'=>'required']);

        if ($validator->fails()){
            return response()->json(['message'=>'body is required'],422);
        }

        $loggers = new Loggers();

        $url1 = $loggers->sms_url_maker($number,$body,'api_1');
        $url2 = $loggers->sms_url_maker($number,$body,'api_2');

        $log_id = $loggers->log_sms($number,$body,0);

        $api_caller = new ApiCaller();

        if ($api_caller->api_call($url1,'api_1')){

            $loggers->update_sms_log($log_id,1);
            $loggers->log_api_usage('api_1',1,$log_id,'');

            return response()->json(['message'=>'sms was sent!'],200);

        } elseif ($api_caller->api_call($url2,'api_2')) {

            $loggers->update_sms_log($log_id,1);
            $loggers->log_api_usage('api_1',0,$log_id,$api_caller->error[0]);
            $loggers->log_api_usage('api_2',1,$log_id,'');

            return response()->json(['message'=>'sms was sent!'],200);

        } else {

            $loggers->log_api_usage('api_1',0,$log_id,$api_caller->error[0]);
            $loggers->log_api_usage('api_2',0,$log_id,$api_caller->error[1]);

            SendFailedSms::dispatch();
            return response()->json(['message'=>$api_caller->error],503);

        }

    }

    public function index(Request $request)
    {

        $number = $request->number;

        if (!$sent_sms_count = Redis::get('sent_sms_count')){

            $sent_sms_count = Log::where('status',1)->count();
            Redis::setex('sent_sms_count',300,$sent_sms_count);

        }

        if (!$api_1_usage_count = Redis::get('api_1_usage_count')){

            $api_1_usage_count = Api_usage::where('api_name','api_1')->count();
            Redis::setex('api_1_usage_count',300,$api_1_usage_count);

        }

        if (!$api_1_fails_count = Redis::get('api_1_fails_count')){

            $api_1_fails_count = Api_usage::where([['api_name','api_1'],['status',0]])->count();
            Redis::setex('api_1_fails_count',300,$api_1_fails_count);

        }

        if (!$api_2_usage_count = Redis::get('api_2_usage_count')){

            $api_2_usage_count = Api_usage::where('api_name','api_2')->count();
            Redis::setex('api_2_usage_count',300,$api_2_usage_count);

        }

        if (!$api_2_fails_count = Redis::get('api_2_fails_count')){

            $api_2_fails_count = Api_usage::where([['api_name','api_2'],['status',0]])->count();
            Redis::setex('api_2_fails_count',300,$api_2_fails_count);

        }

        if (!$top_10_used_numbers = json_decode(Redis::get('top_10_used_numbers'))){

            $top_10_used_numbers = DB::table('logs')
                ->select(DB::raw('distinct(number), count(number) as number_count'))
                ->where('status',1)
                ->limit(10)
                ->groupBy('number')
                ->orderBy('number_count','desc')
                ->get();
            Redis::setex('top_10_used_numbers',300,json_encode($top_10_used_numbers));

        }

        $logs = Log::query();

        if (!empty($number)){
            $logs->where('number',$number);
        }

        $logs = $logs->paginate(5,['id','number','status','updated_at']);

        $api_1_fail_percentage = round(($api_1_fails_count/$api_1_usage_count)*100,1);

        $api_2_fail_percentage = round(($api_2_fails_count/$api_2_usage_count)*100,1);

        return view('reports',[
            'sent_sms_count'=>$sent_sms_count,
            'api_1_usage_count'=>$api_1_usage_count,
            'api_1_fail_percentage'=>$api_1_fail_percentage,
            'api_2_usage_count'=>$api_2_usage_count,
            'api_2_fail_percentage'=>$api_2_fail_percentage,
            'top_10_userd_numbers'=>$top_10_used_numbers,
            'logs'=>$logs
        ]);

    }

}
