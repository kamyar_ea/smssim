<?php

namespace App\Jobs;

use App\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Helpers\ApiCaller;
use App\Helpers\Loggers;

class SendFailedSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $logs = Log::select('id','number','body')->where('status',0)->get();

        if (count($logs)>0){

            $loggers = new Loggers();

            $api_caller = new ApiCaller();

            foreach ($logs as $log){

                $number = $log->number;
                $body = $log->body;
                $log_id = $log->id;

                $url1 = $loggers->sms_url_maker($number,$body,'api_1');
                $url2 = $loggers->sms_url_maker($number,$body,'api_2');

                if ($api_caller->api_call($url1,'api_1')){

                    $loggers->update_sms_log($log_id,1);
                    $loggers->log_api_usage('api_1',1,$log_id,'');

                } elseif ($api_caller->api_call($url2,'api_2')) {

                    $loggers->update_sms_log($log_id,1);
                    $loggers->log_api_usage('api_1',0,$log_id,$api_caller->error[0]);
                    $loggers->log_api_usage('api_2',1,$log_id,'');

                } else {

                    $loggers->log_api_usage('api_1',0,$log_id,$api_caller->error[0]);
                    $loggers->log_api_usage('api_2',0,$log_id,$api_caller->error[1]);

                }

            }

        }

        $failed_logs = Log::select('id','number','body')->where('status',0)->get();

        if (count($failed_logs)>0){

            SendFailedSms::dispatch()->delay(now()->addMinutes(30));

        }

    }
}
