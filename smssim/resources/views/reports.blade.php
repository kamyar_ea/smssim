<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Reports</title>
    <script src="{{asset('js/app.js')}}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div class="container">
        <h2 class="text-center">Reports</h2>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <table border="1">
                    <thead>
                    <tr>
                        <td>Total Sent Messages</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$sent_sms_count}} Messages</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-3">
                <table border="1">
                    <thead>
                    <tr>
                        <td colspan="2">Api Usage Count</td>
                    </tr>
                    <tr>
                        <td>Api 1</td>
                        <td>Api 2</td>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$api_1_usage_count}} Attempts</td>
                            <td>{{$api_2_usage_count}} Attempts</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-3">
                <table border="1">
                    <thead>
                    <tr>
                        <td colspan="2">Api Fail Percentage</td>
                    </tr>
                    <tr>
                        <td>Api 1</td>
                        <td>Api 2</td>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$api_1_fail_percentage}} %</td>
                            <td>{{$api_2_fail_percentage}} %</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-3">
                <table border="1">
                    <thead>
                    <tr>
                        <td colspan="2">Top 10 Sent Numbers</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td>Count</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($top_10_userd_numbers as $number)
                        <tr>
                            <td>{{$number->number}}</td>
                            <td>{{$number->number_count}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <form method="get" action="">
                        <input type="text" placeholder="number to search" name="number" value="{{app('request')->input('number')}}" size="20">
                        <input type="submit" value="search">
                    </form>
                </div>
                <table border="1">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Number</td>
                            <td>Status</td>
                            <td>Last Attempt Time</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($logs as $log)
                            <tr>
                                <td>{{$log->id}}</td>
                                <td>{{$log->number}}</td>
                                <td>{{$log->status}}</td>
                                <td>{{$log->updated_at}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @if(app('request')->input('number'))
                    {{$logs->appends(['number' => app('request')->input('number')])->links('')}}
                @else
                    {{$logs->links('')}}
                @endif
            </div>
        </div>
    </div>
</body>
</html>